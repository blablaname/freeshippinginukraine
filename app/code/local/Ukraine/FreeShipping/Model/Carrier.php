<?php

class Ukraine_FreeShipping_Model_Carrier {

    public function shipping() {
        /* @var $result Mage_Shipping_Model_Rate_Result */
        $result = Mage::getModel('shipping/rate_result');
        $country = Mage::getStoreConfig('shipping/origin/country_id');
        if("Ukraine" == $country || "UA" == $country || "UKR" == $country) {
            $freeShippingRate = $this->_getFreeShippingRate();
            $result->append($freeShippingRate);
        }
    }

    protected function _getFreeShippingRate() {
        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
        $rate = Mage::getModel('shipping/rate_result_method');
        $rate->setPrice(0);
        $rate->setCost(0);
        return $rate;
    }
}